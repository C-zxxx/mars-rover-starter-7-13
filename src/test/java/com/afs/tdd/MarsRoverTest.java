package com.afs.tdd;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class MarsRoverTest {
    @Test
    void should_change_Location_to_0_1_North_when_executeCommand_given_location_0_0_North_and_command_Move() {
        //given
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(Command.Move);

        //then
        assertEquals(0, marsRover.getLocation().getCoordinateX());
        assertEquals(1, marsRover.getLocation().getCoordinateY());
        assertEquals(Direction.North, marsRover.getLocation().getDirection());
    }

    @Test
    void should_change_Location_to_0_0_West_when_executeCommand_given_location_0_0_North_and_command_Left() {
        //given
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(Command.Left);

        //then
        assertEquals(0, marsRover.getLocation().getCoordinateX());
        assertEquals(0, marsRover.getLocation().getCoordinateY());
        assertEquals(Direction.West, marsRover.getLocation().getDirection());
    }

    @Test
    void should_change_Location_to_0_0_East_when_executeCommand_given_location_0_0_North_and_command_Right() {
        //given
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(Command.Right);

        //then
        assertEquals(0, marsRover.getLocation().getCoordinateX());
        assertEquals(0, marsRover.getLocation().getCoordinateY());
        assertEquals(Direction.East, marsRover.getLocation().getDirection());
    }

    @Test
    void should_change_Location_to_0__1_South_when_executeCommand_given_location_0_0_South_and_command_Move() {
        //given
        Location location = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(Command.Move);

        //then
        assertEquals(0, marsRover.getLocation().getCoordinateX());
        assertEquals(-1, marsRover.getLocation().getCoordinateY());
        assertEquals(Direction.South, marsRover.getLocation().getDirection());
    }

    @Test
    void should_change_Location_to_0_0_East_when_executeCommand_given_location_0_0_South_and_command_Left() {
        //given
        Location location = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(Command.Left);

        //then
        assertEquals(0, marsRover.getLocation().getCoordinateX());
        assertEquals(0, marsRover.getLocation().getCoordinateY());
        assertEquals(Direction.East, marsRover.getLocation().getDirection());
    }

    @Test
    void should_change_Location_to_0_0_West_when_executeCommand_given_location_0_0_South_and_command_Right() {
        //given
        Location location = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(Command.Right);

        //then
        assertEquals(0, marsRover.getLocation().getCoordinateX());
        assertEquals(0, marsRover.getLocation().getCoordinateY());
        assertEquals(Direction.West, marsRover.getLocation().getDirection());
    }

    @Test
    void should_change_Location_to_1_0_East_when_executeCommand_given_location_0_0_East_and_command_Move() {
        //given
        Location location = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(Command.Move);

        //then
        assertEquals(1, marsRover.getLocation().getCoordinateX());
        assertEquals(0, marsRover.getLocation().getCoordinateY());
        assertEquals(Direction.East, marsRover.getLocation().getDirection());
    }

    @Test
    void should_change_Location_to_0_0_North_when_executeCommand_given_location_0_0_East_and_command_Left() {
        //given
        Location location = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(Command.Left);

        //then
        assertEquals(0, marsRover.getLocation().getCoordinateX());
        assertEquals(0, marsRover.getLocation().getCoordinateY());
        assertEquals(Direction.North, marsRover.getLocation().getDirection());
    }

    @Test
    void should_change_Location_to_0_0_South_when_executeCommand_given_location_0_0_East_and_command_Right() {
        //given
        Location location = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(Command.Right);

        //then
        assertEquals(0, marsRover.getLocation().getCoordinateX());
        assertEquals(0, marsRover.getLocation().getCoordinateY());
        assertEquals(Direction.South, marsRover.getLocation().getDirection());
    }

    @Test
    void should_change_Location_to__1_0_West_when_executeCommand_given_location_0_0_West_and_command_Move() {
        //given
        Location location = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(Command.Move);

        //then
        assertEquals(-1, marsRover.getLocation().getCoordinateX());
        assertEquals(0, marsRover.getLocation().getCoordinateY());
        assertEquals(Direction.West, marsRover.getLocation().getDirection());
    }

    @Test
    void should_change_Location_to_0_0_South_when_executeCommand_given_location_0_0_West_and_command_Left() {
        //given
        Location location = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(Command.Left);

        //then
        assertEquals(0, marsRover.getLocation().getCoordinateX());
        assertEquals(0, marsRover.getLocation().getCoordinateY());
        assertEquals(Direction.South, marsRover.getLocation().getDirection());
    }

    @Test
    void should_change_Location_to_0_0_North_when_executeCommand_given_location_0_0_West_and_command_Right() {
        //given
        Location location = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(Command.Right);

        //then
        assertEquals(0, marsRover.getLocation().getCoordinateX());
        assertEquals(0, marsRover.getLocation().getCoordinateY());
        assertEquals(Direction.North, marsRover.getLocation().getDirection());
    }


    @Test
    void should_change_Location_to__1_1_North_when_executeBatchCommands_given_location_0_0_North_and_command_MLMR() {
        //given
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeBatchCommands(List.of(Command.Move, Command.Left, Command.Move, Command.Right));

        //then
        assertEquals(-1, marsRover.getLocation().getCoordinateX());
        assertEquals(1, marsRover.getLocation().getCoordinateY());
        assertEquals(Direction.North, marsRover.getLocation().getDirection());
    }

}
