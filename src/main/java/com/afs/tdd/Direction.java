package com.afs.tdd;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public enum Direction {
    West, North, East, South;

    public int getDirectionIndex(Direction direction) {

        AtomicInteger index = new AtomicInteger();
        Stream.iterate(0, i -> i + 1).limit(Direction.values().length).forEach(i -> {
            if (direction.equals(Direction.values()[i])) {
                index.set(i);
            }
        });

        return index.get();

    }
}
