package com.afs.tdd;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MarsRover {
    public static final int[][] STEPS = new int[][]{{-1, 0}, {0, 1}, {1, 0}, {0, -1}};
    private Location location;

    public MarsRover(Location location) {
        this.location = location;
    }

    public void executeCommand(Command command) {
        if (command.equals(Command.Move)) {
            move();
        }
        if (command.equals(Command.Left)) {
            turnLeft();
        }
        if (command.equals(Command.Right)) {
            turnRight();
        }
    }

    private void turnRight() {
        int currentDirectionIndex = location.getDirection().getDirectionIndex(location.getDirection());
        location.setDirection(Direction.values()[(currentDirectionIndex + 1) % 4]);
    }

    private void turnLeft() {
        int currentDirectionIndex = location.getDirection().getDirectionIndex(location.getDirection());
        location.setDirection(Direction.values()[(currentDirectionIndex == 0 ? 3 : currentDirectionIndex - 1)]);
    }

    private void move() {

        int currentDirectionIndex = location.getDirection().getDirectionIndex(location.getDirection());
        location.setCoordinateX(location.getCoordinateX() + STEPS[currentDirectionIndex][0]);
        location.setCoordinateY(location.getCoordinateY() + STEPS[currentDirectionIndex][1]);

    }

    public Location getLocation() {
        return location;
    }

    public void executeBatchCommands(List<Command> commands) {
        commands.forEach(this::executeCommand);
    }
}
